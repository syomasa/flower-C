#include <curses.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define PI 3.14159265
void sleep(long milli);
void draw_circle(int startx, int starty, double angle);

int main() {

    initscr();
    curs_set(0);

    start_color();
    init_pair(1, COLOR_YELLOW, COLOR_BLACK);

    for (int i = 0; i < 360 / 45; i++) {
        int x = round((double)COLS / 2 + 2 * 10 * cos(PI / 4 * i));
        int y = round((double)LINES / 2 + 10 * sin(PI / 4 * i));
        draw_circle(x, y, PI / 384);
    }

    getch();
    endwin();
    return 0;
}

void sleep(long milli) {
    clock_t current = clock();
    while (clock() < current + (CLOCKS_PER_SEC * milli) / 1000)
        ;
}

void draw_circle(int startx, int starty, double angle) {

    double r = 10;
    double phase = 0;
    double x, y;
    if (starty >= LINES - r || starty < 0 + r) {
        move(round(LINES / 2), round(COLS / 2));
        return;
    }
    if (startx >= COLS - r || startx < 0 + r) {
        move(round(LINES / 2), round(COLS / 2));
        return;
    }
    attron(COLOR_PAIR(1));
    while (phase * angle < 2 * PI) {
        // Multiply r * 2 because there ~2 more lines than columns on widescreen
        // terminal otherwise doesn't show up correctly on terminal
        x = 2 * r * cos(angle * phase);
        y = r * sin(angle * phase);
        move(round(starty + y), round(startx + x));
        addch('@');
        refresh();
        fflush(stdout);
        sleep(1);
        phase++;
    }
    attroff(COLOR_PAIR(1));
    move(starty, startx);
}
